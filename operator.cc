/* Nama Program : operator.pas
   Tgl buat     : 3 Oktober 2023
   Deskripsi    : pengoperasian variabel bertipe dasar
   --------------------------------------------------- */
#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int main()
{
    system("clear");

    bool TF = false;
    int i = 0, j = 0;
    float x = 0.0, y = 0.0;
    
    // proses boolean
    cout << "tabel kebenaran" << endl;
    cout << "----------------------------------" << endl;
    cout << "| x1 | x2 | x1 and x2 | x1 or x2 |" << endl;
    cout << "----------------------------------" << endl;
    cout << "| 1  | 1  | " << setw(5) << (true and true) << setw(6) << "|" << setw(5) << (true or true) << setw(6) << "|" << endl;
    cout << "| 1  | 0  | " << setw(5) << (true and false) << setw(6) << "|" << setw(5) << (true or false) << setw(6) << "|" << endl;
    cout << "| 0  | 1  | " << setw(5) << (false and true) << setw(6) << "|" << setw(5) << (false or true) << setw(6) << "|" << endl;
    cout << "| 0  | 0  | " << setw(5) << (false and false) << setw(6) << "|" << setw(5) << (false or false) << setw(6) << "|" << endl;
    cout << "----------------------------------" << endl;

    // proses operasi numerik
    i = 5;
    j = 2;
    cout << "operasi numerik pada tipe data integer" << endl;
    cout << "---------------------------" << endl;
    cout << "| operasi | hasil operasi |" << endl;
    cout << "---------------------------" << endl;
    cout << "| " << i << " + " << j << "   | " << setw(8) << (i + j) << setw(7) << "|" << endl;
    cout << "| " << i << " - " << j << "   | " << setw(8) << (i - j) << setw(7) << "|" << endl;
    cout << "| " << i << " * " << j << "   | " << setw(8) << (i * j) << setw(7) << "|" << endl;
    cout << "| " << i << " / " << j << "   | " << setw(8) << (i / j) << setw(7) << "|" << endl;
    cout << "| " << i << " div " << j << " | " << setw(8) << (i / j) << setw(7) << "|" << endl;
    cout << "| " << i << " mod " << j << " | " << setw(8) << (i % j) << setw(7) << "|" << endl;
    cout << "---------------------------" << endl;

    x = 5.0;
    y = 2.0;
    cout << "operasi numerik pada tipe data float" << endl;
    cout << "---------------------------" << endl;
    cout << "| operasi | hasil operasi |" << endl;
    cout << "---------------------------" << endl;
    cout << "| " << setprecision(2) << x << " + " << y << setw(5) << "| " << setw(6) << (x + y) << setw(10) << "| " << endl;
    cout << "| " << setprecision(2) << x << " - " << y << setw(5) << "| " << setw(6) << (x - y) << setw(10) << "| " << endl;
    cout << "| " << setprecision(2) << x << " * " << y << setw(5) << "| " << setw(6) << (x * y) << setw(10) << "| " << endl;
    cout << "| " << setprecision(2) << x << " / " << y << setw(5) << "| " << setw(8) << (x / y) << setw(8) << "| " << endl;
    cout << "---------------------------" << endl;

    // operator relasi numerik
    TF = x != y;
    cout << "5.0 != 2.0 adalah=" << TF << endl;
    TF = x < y;
    cout << "5.0 < 2.0 adalah=" << TF << endl;
    return 0;
}
