/*
Nama  :Intan Telaumbanua
Nim   :301230016
kelas :1B Teknik Informatika
*/

#include <iostream>
#include <iomanip>

int main() {
    int N;

    //Meminta input N dari pengguna 
    std::cout << "Masukkan nilai N: ";
    std::cin >> N;

    //Membuat segitiga pascal
    for (int i=0; i<N; i++) {
       //Menambahkan spasi diawal setiap baris
        for (int j=0; j<N-i-1; j++){
            std::cout <<" ";
        }

        int nilai = 1;
        for(int j=0; j<=i; j++) {
            //Mencetak angka  dengan lebar 2 digit
            std::cout << std::setw(2) << nilai <<"";
            nilai = nilai * (i - j)/(j + 1);
        }
        std::cout << std::endl;
    }
    return 0;
}
