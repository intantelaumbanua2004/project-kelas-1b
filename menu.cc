/*
   Nama Program : menu.cc
   Tgl buat     : 18 Oktober 2023
   Deskripsi    : membuat menu
*/

#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int main()
{
  system("clear");

  const float pi = 3.14;
  int pilih = 0;
  float sisi = 0.0, jari = 0.0, tinggi = 0.0;

  cout << "        <<< Menu >>>        " << endl << endl;

  cout << "1. Menghitung Isi Kubus" << endl;
  cout << "2. Menghitung Luas Lingkaran" << endl;
  cout << "3. Menghitung Isi Silisnde" << endl << endl;

  cout << "Pilih Nomor : "; cin >> pilih;

  switch (pilih)
  {
  case 1:
    cout << "Panjang Sisi Kubus : "; cin >> sisi;
    cout << "Isi Kubus : " << setprecision(2) << (sisi * sisi * sisi) << endl;
    break;
  case 2:
    cout << "Jari-jari lingkaran : "; cin >> jari;
    cout << "Luas Lingkaran : " << setprecision(2) << (pi * jari * jari) << endl;
    break;
  case 3:
    cout << "Jari-jari lingkaran : "; cin >> jari;
    cout << "Tinggi Silinder : "; cin >> tinggi;
    cout << "Isi Silinder : " << setprecision(2) << (pi * jari * jari * tinggi) << endl;
    break;
  }

  return 0;
}
