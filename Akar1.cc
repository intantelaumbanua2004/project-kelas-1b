/*
   Nama Program : Akar1.cc
   Tgl buat     : 18 Oktober 2023
   Deskripsi    : menentukan jenis akar persamaan kuadrat
*/

#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int main()
{
  system("clear");

  float a = 0.0, b = 0.0, c = 0.0, D = 0.0;

  cout << "Menentukan Jenis Akar Persamaan Kuadrat" << endl;
  cout << " Persamaan Umum : ax ^ 2 + bx + c " << endl<<endl;

  cout << "a = ";
  cin >> a;
  cout << "b = ";
  cin >> b;
  cout << "c = ";
  cin >> c;
  cout << endl;

  D = (b * b) - (4 * a * c);

  cout << "Nilai Diskriminan : " << setprecision(2) << D << endl;
  cout << "Jenis Akar : " << endl;

  if (D == 0)
  {
    cout << "Kembar" << endl;
  }
  else
  {
    if (D > 0)
    {
      cout << "Berlainan" << endl;
    }
    else
    {
      if (D < 0)
      {
        cout << "imajiner berlainan" << endl;
      }
    }
  }

  return 0;
}
