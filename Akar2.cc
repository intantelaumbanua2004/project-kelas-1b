/*
   Nama Program : Akar2.cc
   Tgl buat     : 18 Oktober 2023
   Deskripsi    : mencari akar persamaan kuadrat
*/

#include <iostream>
#include <iomanip>
#include <cmath>
#include <stdlib.h>

using namespace std;

int main()
{
  system("clear");

  float a = 0.0, b = 0.0, c = 0.0, D = 0.0, x1 = 0.0, x2 = 0.0;

  cout << "     Mencari Akar Persamaan Kuadrat    " << endl;
  cout << "      Persamaan Umum : ax ^ 2 + bx + c " << endl << endl;

  cout << "a = ";
  cin >> a;
  cout << "b = ";
  cin >> b;
  cout << "c = ";
  cin >> c;
  cout << endl;

  D = (b * b) - (4 * a * c);

  cout << "Nilai Diskriminan : " << setprecision(2) << D << endl;
  cout << "Jenis Akar : " << endl;

  if (D == 0)
  {
    x1 = (-b) / (2 * a);
    cout << "Kembar" << endl;
    cout << "x1 = x2 = " << x1 << endl;
  }
  else
  {
    if (D > 0)
    {
      x1 = ((-b) + sqrt(D)) / (2 * a);
      x2 = ((-b) + sqrt(D)) / (2 * a);
      cout << "Berlainan" << endl;
      cout << "x1 = " << setprecision(2) << x1 << endl;
      cout << "x2 = " << setprecision(2) << x2 << endl;
    }
    else
    {
      if (D < 0)
      {
        x1 = (-b) / (2 * a);
        x2 = sqrt(-D) / (2 * a);
        cout << "imajiner berlainan" << endl;
        cout << "x1 =  " << setprecision(2) << x1 << " + " << setprecision(2) << x2 << "i" << endl;
        cout << "x1 =  " << setprecision(2) << x1 << " - " << setprecision(2) << x2 << "i" << endl;
      }
    }
  }

  return 0;
}
