/*
   Nama Program : tunja2.cc
   Tgl buat     : 10 Oktober 2023
   Deskripsi    : menghitung besarnya jumlah tunjangan dan
                  potongan
*/

#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    system("clear");

    int JumlahAnak=0;
    float GajiKotor=0.0,Tunjangan=0.0,PersenTunjangan=0.0,PersenPotongan=0.0;
    float Potongan=0.0;

    PersenTunjangan=0.2;
    PersenPotongan=0.05;
    cout << "Gaji Kotor ? "; cin >> GajiKotor;
    cout << "Jumlah Anak ? "; cin >> JumlahAnak;
    if(JumlahAnak>2)
    {
      PersenTunjangan=0.3;
      PersenPotongan=0.07;
    }
    Tunjangan = PersenTunjangan*GajiKotor;
    Potongan = PersenPotongan*GajiKotor;
    cout << "Besar Tunjangan = Rp " << setprecision(2) << Tunjangan << endl;
    cout << "Besar Potongan = Rp " << setprecision(2) << Potongan << endl;
    
    return 0;
}
